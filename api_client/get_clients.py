#! /usr/local/bin/python
'''
Created on Oct 2, 2014

@author: felixlipov
'''
import sys

from enertiv_oauth_client import EnertivOAuthClient


def main(argv):
  if len(argv) != 4:
    print "usage: get_clients <user> <pw> <client_id> <client_secret>"
    sys.exit(-1)
    
  client = EnertivOAuthClient(argv[0], argv[1], "https", "api.enertiv.com", 443, argv[2], argv[3])
  response = client.get("https://api.enertiv.com/api/client")
  
  print response.json()

if __name__ == "__main__":
  main(sys.argv[1:])