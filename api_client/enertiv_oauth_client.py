import logging
from os.path import expanduser

import requests
from requests.exceptions import Timeout

logger = logging.getLogger("enertiv_client")
logger.setLevel(logging.INFO)

class LoginException(Exception):
  def __init__(self, *args, **kwargs):
    Exception.__init__(self, *args, **kwargs)

def enertiv_server_login(func):
  def wrapper(*arg):
    if arg[0].access_token == None:
      arg[0].login()
    try:
      res = func(*arg)
    except LoginException as e:
      logger.warn("Relogging in %s", e)
      arg[0].session = requests.session()
      response = arg[0].login()
      if response.status_code != 200:
        logger.warn("Relogon failed. response %s %s", response.status_code, response.text)
      res = func(*arg)
    return res
  return wrapper

class EnertivOAuthClient():
  access_token = None
  timeout = 120
  
  def __init__(self, server_user, server_password, 
               server_protocol, server_host, server_port,
               client_id, client_secret):
    self.server_user = server_user
    self.server_password = server_password
    self.server_protocol = server_protocol
    self.server_host = server_host
    self.server_port = server_port
    self.client_id = client_id
    self.client_secret = client_secret
    self.login_url = "%s://%s:%s/oauth2/access_token/" % (server_protocol, server_host, server_port)
    self.session = None
    self.access_token = self.get_token() 

  def set_token(self, access_token):
    self.access_token = access_token
    home = expanduser("~")
    with open(home+'/.enertiv', 'w') as config:
      config.write(self.access_token)
    
  def get_token(self):
    try:
      home = expanduser("~")
      with open(home+'/.enertiv', 'r') as config:
        self.access_token = config.readline()
    except Exception:
      logger.debug("No existing saved token")
      pass

  def login(self):
    try:
      self.session = requests.session()
      self.session.stream = False
      login_data = dict(username=self.server_user, password=self.server_password, client_id=self.client_id, client_secret=self.client_secret, grant_type="password")
      response = self.session.post(self.login_url, data=login_data, timeout=self.timeout, verify=False)
      if (response.status_code != 200):
        logger.info("login result %s %s", response.status_code, response.text)
      else:
        self.set_token(response.json()['access_token'])
        
    except Timeout as e:
      logger.warning('Login for %s timed out: %s' , self.login_url, e)
      raise e
    except Exception as e:
      logger.error(e)
      raise e
    return response
  
  @enertiv_server_login
  def get(self, url):
    return self.request("GET", url)
  
  @enertiv_server_login
  def delete(self, url):
    return self.request("DELETE", url)
  
  @enertiv_server_login
  def post(self, url, data = {}, files = {}):
    return self.request("POST", url, data, files)
  
  @enertiv_server_login
  def request(self, method, url, data = {}, files = None):
    post_values = data
    headers = {"Authorization": "Bearer %s" % self.access_token}
    try:
      response = self.session.request(method, url, data=post_values, 
                                      files=files, headers=headers, 
                                      timeout=self.timeout, verify=False)
    except Timeout as e:
      logger.warning('POST for %s timed out: %s', url, e)
      raise e

    if response.status_code == requests.codes['forbidden']:
      raise LoginException("forbidden status code received on %s %s %s", method, response.status_code, response.text)
    elif (response.status_code != requests.codes['ok']):
      self.access_token = None
      logger.error("raised exception, resetting token %s", response.status_code)
    # read content to let it know we are done with
    response.content
    return response