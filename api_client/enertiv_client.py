import logging

import requests
from requests.exceptions import Timeout

logger = logging.getLogger("enertiv_client")
logger.setLevel(logging.INFO)

class LoginException(Exception):
  def __init__(self, *args, **kwargs):
    Exception.__init__(self, *args, **kwargs)

def enertiv_server_login(func):
  def wrapper(*arg):
      if arg[0].csrf_token == None:
        arg[0].login()
      try:
        res = func(*arg)
      except LoginException as e:
        logger.warn("Relogging in %s" % e)
        arg[0].session = requests.session()
        response = arg[0].login()
        if response.status_code != 200:
          logger.warn("Relogon failed. response %s %s" % (response.status_code, response.text))
        res = func(*arg)
      return res
  return wrapper

class EnertivClient():
  csrf_token = None
  timeout = 120
  
  def __init__(self, app_name, server_user, server_password, 
               server_protocol, server_host, server_port):
    self.app_name = app_name
    self.server_user = server_user
    self.server_password = server_password
    self.server_protocol = server_protocol
    self.server_host = server_host
    self.server_port = server_port
    self.login_url = "%s://%s:%s/%s/login/" % (self.server_protocol, self.server_host, self.server_port, self.app_name)
    self.logout_url = "%s://%s:%s/%s/login/" % (self.server_protocol, self.server_host, self.server_port, self.app_name)
    self.session = None

  def login(self):
    try:
      self.session = requests.session()
      self.session.stream = False
      self.session.get(self.login_url, verify=False, timeout=self.timeout)
      self.csrf_token = self.session.cookies['csrftoken']
      login_data = dict(username=self.server_user, password=self.server_password, csrfmiddlewaretoken=self.csrf_token)
      headers = {"X-CSRFToken": self.csrf_token, "Referer": "%s://%s" % (self.server_protocol, self.server_host)}
      response = self.session.post(self.login_url, data=login_data, headers=headers, timeout=self.timeout, verify=False)
      if (response.status_code != 200):
        logger.info("login result %s %s" % (response.status_code, response.text))
    except Timeout as e:
      logger.warning('Login for %s timed out: %s' % (self.login_url, e))
      raise e
    except Exception as e:
      logger.error(e)
      raise e
    return response
  
  @enertiv_server_login
  def get(self, url):
    return self.request("GET", url)
  
  @enertiv_server_login
  def delete(self, url):
    return self.request("DELETE", url)
  
  @enertiv_server_login
  def post(self, url, data = {}, files = {}):
    return self.request("POST", url, data, files)
  
  @enertiv_server_login
  def request(self, method, url, data = {}, files = None):
    post_values = data
    post_values['csrfmiddlewaretoken'] = self.csrf_token
    if 'sessionid' not in self.session.cookies:
      self.login()
    sessionid = self.session.cookies['sessionid'] 
    self.session.cookies.clear()
    self.session.cookies['csrftoken'] = self.csrf_token
    self.session.cookies['sessionid'] = sessionid
    headers = {"X-CSRFToken": self.csrf_token, "Referer": "%s://%s" % (self.server_protocol, self.server_host)}
    try:
      response = self.session.request(method, url, data=post_values, 
                                      files=files, headers=headers, 
                                      timeout=self.timeout, verify=False)
    except Timeout as e:
      logger.warning('POST for %s timed out: %s' % (url, e))
      raise e

    #if (response.status_code == requests.codes['not_found'] or response.status_code == requests.codes['internal_server_error']):
    if response.status_code == requests.codes['forbidden']:
      raise LoginException("forbidden status code received on %s %s %s" % (method, response.status_code, response.text))
    elif (response.status_code != requests.codes['ok']):
      self.csrf_token = None
      logger.error("raised exception, resetting token %s" % response.status_code)
      #raise Exception("invalid status code received on %s %s %s" % (method, response.status_code, response.text))
    # read content to let it know we are done with
    response.content
    return response