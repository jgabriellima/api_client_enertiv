from setuptools import setup


setup(
    name='api_client',
    packages=['api_client'],
    version='0.1.0',
    description='Enertiv API Sample Client for accessing data via Session or OAuth based authentication using Python.',
    author='Felix Lipov',
    author_email='felix@enertiv.com',
    url='https://bitbucket.org/enertiv/api_client',
    classifiers=[
        'Programming Language :: Python :: 2.7'
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Topic :: Software Development :: Testing',
    ],
    install_requires=[
        'requests',
    ]
)